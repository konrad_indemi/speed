# Repo URL
https://gitlab.com/konrad_indemi/speed

# Composer
composer install

# Setup DB configuration in .env file
DATABASE_URL=mysql://root:@127.0.0.1:3306/speed?serverVersion=5.7

# Create database and run migrations
bin\console doctrine:database:create

bin\console doctrine:migration:migrate

# Run server
symfony serve
