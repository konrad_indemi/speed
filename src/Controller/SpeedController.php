<?php

namespace App\Controller;

use App\Services\TripsStatService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SpeedController
 * @package App\Controller
 */
class SpeedController extends AbstractController
{
    /**
     * @Route(path="/", name="speed")
     * @param TripsStatService $tripsStatService
     * @return Response
     */
    public function main(TripsStatService $tripsStatService): Response
    {
        return $this->render('speed/speed.html.twig', [
            'stats' => $tripsStatService->getTripStats()
        ]);
    }
}
