<?php

namespace App\Services;

use App\Repository\TripsRepository;
use App\Utilities\TripsStatUtility;

/**
 * Class TripsStatService
 * @package App\Services
 */
class TripsStatService
{
    /**
     * @var TripsRepository
     */
    private $tripsRepository;

    /**
     * TripsStatService constructor.
     * @param $tripsRepository
     */
    public function __construct(TripsRepository $tripsRepository)
    {
        $this->tripsRepository = $tripsRepository;
    }

    /**
     * @return array
     */
    public function getTripStats()
    {
        $stats = [];
        foreach ($this->tripsRepository->findAll() as $trip) {
            $tripStatUtility = new TripsStatUtility();
            array_push($stats, $tripStatUtility->getTripStat($trip));
        }
        return $stats;
    }
}
