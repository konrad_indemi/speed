<?php

namespace App\Utilities;

use App\Entity\Trips;

/**
 * Class TripsStatUtility
 * @package App\Utilities
 */
class TripsStatUtility
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var float
     */
    public $distance;
    /**
     * @var integer
     */
    public $measure_interval;
    /**
     * @var float
     */
    public $avg_speed;

    /**
     * @param Trips $trip
     * @return TripsStatUtility
     */
    public function getTripStat(Trips $trip)
    {
        $avg_speeds = [];
        $total_distance = 0;
        if ($trip->getTripMeasures()->count() > 1) {
            foreach ($trip->getTripMeasures() as $i => $tripMeasure) {
                if (!empty($trip->getTripMeasures()->get($i + 1))) {
                    $nextTripMeasure = $trip->getTripMeasures()->get($i + 1);
                    $diff = $nextTripMeasure->getDistance() - $tripMeasure->getDistance();
                    $total_distance = $nextTripMeasure->getDistance();
                    $avg = floor($this->calculateAvgSpeed($diff, $trip->getMeasureInterval()));
                    array_push($avg_speeds, $avg);
                }
            }
        } else {
            array_push($avg_speeds, 0);
        }

        $max_avg_speed = 0;
        if (!empty($avg_speeds)) {
            $max_avg_speed = max($avg_speeds);
        }

        $this->name = $trip->getName();
        $this->distance = $total_distance;
        $this->measure_interval = $trip->getMeasureInterval();
        $this->avg_speed = $max_avg_speed;
        return $this;
    }

    /**
     * @param $distance
     * @param $time
     * @return float|int
     */
    private function calculateAvgSpeed($distance, $time)
    {
        return 3600 * $distance / $time;
    }
}
